Readme file for the Promotion Blocks module for Drupal
---------------------------------------------
This module makes a custom content type 'promotion' on to which you can
insert contents and make it visible in a block with jquery.cycle
animations.

Download latest version of jquery.cycle.all.js from
  url: http://malsup.github.io/jquery.cycle.all.js.

Create a library folder 'jquery.cycle' inside libraries and paste
  the js file (with name jquery.cycle.all.js) inside the folder.

Once you enable the module, a block that lists all the data of this
content type is created. You can insert the promotion content using the
add content button in the back-end.

Then goto the structure > blocks, you will see a block named 'Promotion Blocks'.
Set this block to your desired location. You will see the block having
all promotion type content displayed by jquery cycle animation.
